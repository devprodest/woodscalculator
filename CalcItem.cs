﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System.Collections.Generic;

namespace WoodsCalculator
{
    /// <summary>
    /// Настройки документа
    /// </summary>

    public class MatrixSolver
    {
        public Wood WoodPath { get; set; }

        internal List<Component> Components { get; set; } = new List<Component>();

        public MatrixSolver() => WoodPath = new Wood();

        public MatrixSolver( List<Component> ms )
        {
            foreach ( Component msc in ms )
            {
                var c = new Component
                {
                    Length = msc.Length,
                    Quantity = msc.Quantity
                };
                Components.Add( c );
            }
            WoodPath = new Wood();
        }

        public MatrixSolver( MatrixSolver ms )
        {
            foreach ( Component msc in ms.Components )
            {
                var c = new Component
                {
                    Length = msc.Length
                };
                c.Length = msc.Length;
                Components.Add( c );
            }
            WoodPath = new Wood( ms.WoodPath.Segments );
        }

        public void Sort() => Components.Sort( ( emp1, emp2 ) => emp2.Length.CompareTo( emp1.Length ) );

        public void Add( Component c )
        {
            Components.Add( c );
            Sort();
        }

    }
}

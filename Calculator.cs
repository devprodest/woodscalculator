﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System.Collections.Generic;
using System.Linq;

namespace WoodsCalculator
{
    internal class Calculator
    {
        public Calculator( int woodlength, int woodcut )
        {
            Woodlength = woodlength;
            Woodcut = woodcut;
        }

        public Calculator()
        {
            Woodlength = 6000;
            Woodcut = 6;
        }

        public int Woodlength { get; set; }
        public int Woodcut { get; set; }

        public List<Wood> Solver( List<Component> components )
        {

            var calcitm = new MatrixSolver( components.OrderByDescending( u => u.Length ).ToList() );
            var res = new List<Wood>();

            var flagfind = true;

            while ( flagfind )
            {
                (bool findres, MatrixSolver msolver) = FindOptimalPatch( calcitm );

                if ( findres )
                {
                    res.Add( new Wood( msolver.WoodPath.Segments ) );
                    calcitm = msolver;
                    calcitm.WoodPath.Clear();
                }


                flagfind = false;
                foreach ( Component c in calcitm.Components )
                {
                    if ( c.Quantity > 0 )
                    {
                        flagfind = true;
                        break;
                    }
                }

            }

            return res;
        }

        public (bool findres, MatrixSolver msolver) FindOptimalPatch( MatrixSolver calcitm )
        {
            foreach ( Component component in calcitm.Components )
            {
                if ( component.Quantity == 0 ) continue;

                var newlength = component.Length + calcitm.WoodPath.Length + Woodcut;

                if ( newlength < Woodlength )
                {
                    component.Quantity--;

                    calcitm.WoodPath.Add( component.Length );
                    (bool findres, MatrixSolver msolver) = FindOptimalPatch( calcitm );

                    return (true, findres ? msolver : calcitm);

                }
            }
            return (false, null);
        }
    }
}

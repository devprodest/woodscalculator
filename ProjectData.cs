﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace WoodsCalculator
{
    [Serializable]
    [XmlType( "ProjectData" )]
    [XmlInclude( typeof( ProjectData ) )]
    public class ProjectData
    {
        private decimal trimmingLength = 10;
        private decimal cutLength = 6;
        private decimal woodLengtch = 6000;
        private string projectName = "";
        private List<Wood> solvedWoods = new List<Wood>();
        private List<Component> components = new List<Component>();

        [XmlElement( "ProjectName" )]
        public string ProjectName
        {
            get => projectName; set { projectName = value; Modify = true; }
        }

        [XmlElement( "WoodLengtch" )]
        public decimal WoodLengtch
        {
            get => woodLengtch; set { woodLengtch = value; Modify = true; }
        }

        [XmlElement( "CutLength" )]
        public decimal CutLength
        {
            get => cutLength; set { cutLength = value; Modify = true; }
        }

        [XmlElement( "TrimmingLength" )]
        public decimal TrimmingLength { get => trimmingLength; set { trimmingLength = value; Modify = true; } }

        [XmlArray( "SolvedWoods" )]
        [XmlArrayItem( "Wood" )]
        public List<Wood> SolvedWoods
        {
            get => solvedWoods; set { solvedWoods = value; Modify = true; }
        }

        [XmlArray( "Components" )]
        [XmlArrayItem( "Component" )]
        public List<Component> Components
        {
            get => components; set { components = value; Modify = true; }
        }


        [XmlIgnore]
        public bool Modify { get; set; } = false;

        public void AddComponent( Component c )
        {
            Components.Add( c );
            Modify = true;
        }


        public void Serialize( string str )
        {
            Type[] Types = { typeof( ProjectData ), typeof( Component ), typeof( Wood ) };

            var serializer = new XmlSerializer( typeof( ProjectData ), Types );
            using ( var fs = new FileStream( str, FileMode.Create ) )
            {
                serializer.Serialize( fs, this );
            }
        }


        public bool Serialize()
        {
            Modify = false;

            if ( string.IsNullOrEmpty( ProjectName ) ) return false;
            Serialize( ProjectName );
            return true;
        }

        public bool DeSerialize( string str )
        {
            if ( !File.Exists( str ) ) return false;
            var proj = new ProjectData();

            Type[] Types = { typeof( ProjectData ), typeof( Component ), typeof( Wood ) };

            var serializer = new XmlSerializer( typeof( ProjectData ), Types );
            //BinaryFormatter  serializer = new BinaryFormatter();
            using ( var fs = new FileStream( str, FileMode.Open ) )
            {
                proj = (ProjectData)serializer.Deserialize( fs );
            }

            ProjectName = str;
            WoodLengtch = proj.WoodLengtch;
            CutLength = proj.CutLength;
            TrimmingLength = proj.TrimmingLength;
            SolvedWoods = proj.SolvedWoods;
            Components = proj.Components;
            Modify = false;


            return true;
        }

    }
}

﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace WoodsCalculator
{
    public partial class WoodsCalcForm : Form
    {
        private ProjectData proj = new ProjectData();

        public WoodsCalcForm()
        {
            InitializeComponent();
            projectDataBindingSource.DataSource = proj;
        }


        private void btnAddSegment_Click( object sender, EventArgs e )
        {
            btnDelSegment.Enabled = true;
            proj.AddComponent( new Component( 0, 0 ) );
            ShowData();
        }

        private void ShowData()
        {
            source.Clear();
            foreach ( Component itm in proj.Components )
            {
                source.Add( itm );
            }
            dataGridView1.DataSource = source;
            dataGridView1.Refresh();
        }

        private void btnDelSegment_Click( object sender, EventArgs e )
        {
            if ( proj.Components.Count > 0 )
            {
                proj.Components.RemoveAt( dataGridView1.CurrentCell.RowIndex );
                ShowData();
            }
            btnDelSegment.Enabled = (proj.Components.Count != 0);
        }

        private void Form1_Load( object sender, EventArgs e )
        {
            var args = Environment.GetCommandLineArgs();
            if ( args.Length >= 2 ) proj.DeSerialize( args[ 1 ] );
        }

        private void ВыполнитьToolStripMenuItem_Click( object sender, EventArgs e )
        {
            var calc = new Calculator( (int)(proj.WoodLengtch - proj.TrimmingLength), (int)proj.CutLength );

            proj.SolvedWoods = calc.Solver( proj.Components );

            ResultToText();

        }

        private void ResultToText()
        {
            var str = new StringBuilder();

            if ( proj.SolvedWoods.Count > 0 )
            {
                str.AppendLine( $"Всего досок: {proj.SolvedWoods.Count}\n" );
                var c = 1;
                var lencut = 0;
                foreach ( Wood w in proj.SolvedWoods )
                {
                    str.Append( $"[{c++,2:d}] : " );
                    foreach ( var i in w.Segments )
                    {
                        str.Append( $"{i,5:d} - " );
                    }
                    lencut += (int)proj.WoodLengtch - w.Length;
                    str.Append( $"({ proj.WoodLengtch - w.Length})\n" );
                }
                str.AppendLine( $"Общая длина обрезков: {lencut}" );
                str.AppendLine( $"\n [x] - номер доски\n(xxx) - длина обрезка" );
            }
            else
            {
                str.AppendLine( $"Решений не найдено\n" );
            }
            txtSolution.Text = str.ToString();
        }

        private void СохранитьToolStripMenuItem1_Click( object sender, EventArgs e )
        {
            var sf = new SaveFileDialog
            {
                Filter = "Текстовый документ|*.txt|PDF файл|*.pdf|Excel 2007|*.xlsx|Все файлы|*.*",
                FilterIndex = 1,
                RestoreDirectory = true
            };

            if ( sf.ShowDialog() == DialogResult.OK )
            {
                switch ( sf.FilterIndex )
                {
                    case 1: ///< txt
                    case 4: ///< all
                        SaveTXT( sf.FileName );
                        break;
                    case 2: ///< pdf
                        SavePDF( sf.FileName );
                        break;
                    case 3: ///< xlsx
                        SaveXLSX( sf.FileName );
                        break;
                    default:
                        break;
                }
            }
            txtFileSave.Text = sf.FileName;
        }
        private void SaveTXT( string fileName ) => File.AppendAllText( fileName, txtSolution.Text );

        private void SaveXLSX( string fileName )
        {
            try
            {
                if ( File.Exists( fileName ) ) File.Delete( fileName );
            }
            catch
            {
                fileName = $"new-{fileName}";
                MessageBox.Show( $"Что-то пошло не так.\nФайл будет сохранен как \"{fileName}\"", "Ошибка", MessageBoxButtons.OK );
            }


            using ( var xlPackage = new ExcelPackage( new FileInfo( fileName ) ) )
            {
                ExcelWorksheet excel = xlPackage.Workbook.Worksheets.Add( "Расчет" );

                // Наименования полей общей информации
                ExcelRange CountWoodCell = excel.Cells[ "B1:E1" ];
                ExcelRange CutWoodCell = excel.Cells[ "B2:E2" ];

                CountWoodCell.Merge = true;
                CountWoodCell.Value = "Общее количество досок, шт";
                CutWoodCell.Merge = true;
                CutWoodCell.Value = "Общая длина обрезков, мм";


                // Вычисление размера таблицы
                var maxcountsegment = 0;
                foreach ( Wood w in proj.SolvedWoods ) if ( w.Segments.Count > maxcountsegment ) maxcountsegment = w.Segments.Count;

                const int TABLE_ROW_POSITION = 4;
                var currentrow = TABLE_ROW_POSITION;

                // Стиль ячеек таблицы
                ExcelRange modelTable = excel.Cells[ currentrow, 1, currentrow + proj.SolvedWoods.Count + 1, maxcountsegment + 2 ];
                modelTable.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                modelTable.Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                modelTable.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                modelTable.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                modelTable.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                modelTable.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                /// Заголовок таблицы
                excel.Cells[ currentrow, 1, currentrow + 1, 1 ].Merge = true;
                excel.Cells[ currentrow, 1, currentrow + 1, 1 ].Value = "№";
                excel.Cells[ currentrow, maxcountsegment + 2, currentrow + 1, maxcountsegment + 2 ].Merge = true;
                excel.Cells[ currentrow, maxcountsegment + 2, currentrow + 1, maxcountsegment + 2 ].Value = "Обрезок";
                excel.Cells[ currentrow, 1, currentrow + 1, maxcountsegment + 2 ].Style.Font.Bold = true;

                var lencut = 0;

                if ( maxcountsegment > 0 )
                {
                    excel.Cells[ currentrow, 2, currentrow, maxcountsegment + 1 ].Merge = true;
                    excel.Cells[ currentrow, 2, currentrow, maxcountsegment + 1 ].Value = "Сегменты";
                    currentrow++;

                    for ( var i = 0; i < maxcountsegment; i++ ) excel.Cells[ currentrow, i + 2 ].Value = i + 1;
                    currentrow++;

                    /// Заполенние таблицы

                    foreach ( Wood w in proj.SolvedWoods )
                    {
                        var c = 1;

                        excel.Cells[ currentrow, c++ ].Value = currentrow - TABLE_ROW_POSITION - 1;

                        foreach ( var i in w.Segments ) excel.Cells[ currentrow, c++ ].Value = i;
                        lencut += (int)proj.WoodLengtch - w.Length;

                        excel.Cells[ currentrow, maxcountsegment + 2 ].Value = proj.WoodLengtch - w.Length;
                        currentrow++;
                    }
                }

                excel.Cells[ "A1" ].Value = proj.SolvedWoods.Count;
                excel.Cells[ "A2" ].Value = lencut;

                xlPackage.Save();
            }
        }


        private void SavePDF( string fileName ) => MessageBox.Show( "Функция пока не реализована, используйте другие варианты сохранения" );

        private void СоздатьToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( proj.Modify )
            {
                switch ( MessageBox.Show( "Проект не сохранен, сохранить?", "Проект не сохранен", MessageBoxButtons.YesNoCancel ) )
                {
                    case DialogResult.Cancel:
                        return;
                    case DialogResult.Yes:
                        SaveProject();
                        break;
                    case DialogResult.No:
                        break;
                }
            }

            proj = new ProjectData();
            projectDataBindingSource.DataSource = proj;
            ShowData();
            txtSolution.Text = "";
        }

        private void SaveProject()
        {
            if ( string.IsNullOrEmpty( proj.ProjectName ) )
            {
                var sp = new SaveFileDialog()
                {
                    Filter = "Проект расчета распила досок|*.woodproj|Все файлы|*.*"
                };

                if ( sp.ShowDialog() == DialogResult.OK ) proj.ProjectName = sp.FileName;
            }

            proj.Serialize();
        }


        private void СохранитьToolStripMenuItem_Click( object sender, EventArgs e ) => SaveProject();

        private void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( proj.Modify )
            {
                switch ( MessageBox.Show( "Проект не сохранен, сохранить?", "Проект не сохранен", MessageBoxButtons.YesNoCancel ) )
                {
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                    case DialogResult.Yes:
                        SaveProject();
                        break;
                    case DialogResult.No:
                        break;
                }
            }
        }

        private void ОткрытьToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( proj.Modify )
            {
                switch ( MessageBox.Show( "Проект не сохранен, сохранить?", "Проект не сохранен", MessageBoxButtons.YesNoCancel ) )
                {
                    case DialogResult.Cancel:
                        return;
                    case DialogResult.Yes:
                        SaveProject();
                        break;
                    case DialogResult.No:
                        break;
                }
            }

            var op = new OpenFileDialog()
            {
                Filter = "Проект расчета распила досок|*.woodproj|Все файлы|*.*"
            };

            if ( op.ShowDialog() == DialogResult.OK ) proj.DeSerialize( op.FileName );

            projectDataBindingSource.DataSource = proj;
            ShowData();
            ResultToText();
        }

        private void ОхранитьКакToolStripMenuItem_Click( object sender, EventArgs e )
        {

            var sp = new SaveFileDialog()
            {
                Filter = "Проект расчета распила досок|*.woodproj|Все файлы|*.*"
            };

            if ( sp.ShowDialog() == DialogResult.OK ) proj.Serialize( sp.FileName );
        }

        private void ВыходToolStripMenuItem_Click( object sender, EventArgs e ) => Close();

        private void TxtFileSave_Click( object sender, EventArgs e )
        {
            Process.Start( txtFileSave.Text );
            txtFileSave.Text = "";
        }

        private void ImportProjectDataToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if ( proj.Modify )
            {
                switch ( MessageBox.Show( "Проект не сохранен, сохранить?", "Проект не сохранен", MessageBoxButtons.YesNoCancel ) )
                {
                    case DialogResult.Cancel:
                        return;
                    case DialogResult.Yes:
                        SaveProject();
                        break;
                    case DialogResult.No:
                        break;
                }
            }

            var op = new OpenFileDialog()
            {
                Filter = "CSV файл|*.csv|Все файлы|*.*"
            };

            proj = new ProjectData();

            if ( op.ShowDialog() == DialogResult.OK )
            {
                foreach ( var line in File.ReadLines( op.FileName ) )
                {

                    var s = line.Split( ';' );
                    int.TryParse( s[ 0 ], out var l );
                    int.TryParse( s[ 1 ], out var q );
                    proj.AddComponent( new Component( l, q ) );
                }
            }

            projectDataBindingSource.DataSource = proj;
            ShowData();
            ResultToText();
        }
    }
}

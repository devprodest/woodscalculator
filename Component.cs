﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Xml.Serialization;

namespace WoodsCalculator
{
    [Serializable]
    [XmlType( "Component" )]
    [XmlInclude( typeof( Component ) )]
    public class Component
    {
        [XmlAttribute( "Length" )]
        public int Length { get; set; } = 0;

        [XmlAttribute( "Quantity" )]
        public int Quantity { get; set; } = 0;

        public Component( int len, int qu )
        {
            Length = len;
            Quantity = qu;
        }

        public Component( Component c )
        {
            Length = c.Length;
            Quantity = c.Quantity;
        }

        public Component()
        {
            Length = 0;
            Quantity = 0;
        }

    }
}

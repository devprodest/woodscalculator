﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace WoodsCalculator
{
    [Serializable]
    [XmlType( "Wood" )]
    [XmlInclude( typeof( Wood ) )]
    public class Wood
    {
        [XmlAttribute( "WoodLength" )]
        public int Length { get; set; } = 0;

        [XmlArray( "Segments" )]
        [XmlArrayItem( "Segment" )]
        public List<int> Segments { get; set; }

        public Wood()
        {
            Segments = new List<int>();
            Length = 0;
        }
        public Wood( List<int> st )
        {
            Segments = new List<int>(st);
            foreach ( int i in st ) Length += i;
        }
        public void Clear()
        {
            Segments.Clear();
            Length = 0;
        }
        public void Add( int value )
        {
            Segments.Add( value );
            Length += value;
        }
    }
}
